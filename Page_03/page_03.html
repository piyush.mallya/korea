<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UFT-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Futuristic Modernity</title>
  <link rel="stylesheet" href="../Page_03/blog_page_style_03.css" />
</head>

<body>
  <div class="container">
    <h1>An urbanist’s tour of South Korea: a glimpse into the future of world cities</h1>
    <p>Day one: Welcome to South Korea week, as regular Guardian Cities correspondent Colin Marshall goes under the skin of this most extraordinary country for a series of daily despatches</p>

    <img src="../Page_03/Page_03_Images/page_06.jpeg" />

    <p>Since at least the 1970s, we’ve turned to East Asia for a look at the urban future. Thirty years ago you’d have found it Tokyo, but the prolonged economic malaise that beset Japan after the postwar boom that made its capital such a thrilling vision has turned the country inward. Many westerners now seem convinced that the future – urban, rural, or otherwise – looks, speaks, and (given all that industrialisation) smells Chinese. Yet China’s vast size, still-authoritarian government and enormous ballast of poor people make it, to my mind, an unlikely contender. No, to experience first-hand what lies in store for world cities, you’ll want to book a flight to South Korea.</p>
    <p>First and foremost, I mean Seoul. One can hardly overstate this nation’s capital-centricity, which makes England look like it just happens to have a big city called London. Americans must imagine a metropolitan area that somehow combines New York, Los Angeles, and Washington, DC, swallowing up a handful of other respectably large towns in the process. Ollagada, the Korean-language verb for making a trip to Seoul for a day or a lifetime, translates as “going up”; naeryeogada, the one for returning to your hometown, translates to “going down”. A young South Korean looking to make something of himself has, in recent history, faced two options: go over to America, if possible, or go up to Seoul.</p>
    <p>The capital’s role as absolute centre of gravity hasn’t necessarily benefited the country as a whole, but what a metropolis has resulted. Even the most wayworn city traveller will feel exhilarated, as I did, upon first stepping into Seoul’s subway network, still growing yet already the most extensive in the world. It especially excels in its integration with the city itself – you can obtain pretty much everything you need in life not just without ever leaving the subway, but without ever leaving certain stations.</p>
    <p>In my past month based in Seoul, these and other amenities – the well-maintained restrooms in those stations; the Wi-Fi-equipped coffee shops where none need worry about the safety of their laptop; the other 24-hour businesses of every imaginable kind – have at times made me believe that South Korea has attained a new high watermark of urban civilisation.</p>

    <img src="../Page_03/Page_03_Images/page_07.jpeg" />

    <p>Admittedly, I may be a little biased. First enthused by a burst of creativity in Korean cinema (notably the urban-dwelling comedies of director Hong Sangsoo, often called South Korea’s Woody Allen), I’ve spent years studying Korean language and culture, most recently from my home in Los Angeles’ Koreatown, a neighbourhood Korean friends describe as looking and feeling like a little piece of the Seoul of long ago.</p>
    <p>And despite the best efforts of forward-thinking officials, even the real Seoul hasn’t extinguished its developing-world past. As I sit at the window in a vegan cafe in the international neighborhood of Itaewon, a middle-aged lady passes with a complete Korean meal (including the requisite myriad side dishes) balanced expertly on her head. As I cycle along the Han River, the old-timer ahead of me – fully suited up in lycra riding gear – suddenly pulls over to gather roadside greens. I drink a first-rate cappuccino in a traditional hanok house (all wooden beams and curvy roof), the recent reevaluation of which has got some Seoulites working to spare them demolition – or better yet, in their thinking, to demolish and rebuild them in more technologically advanced ways.</p>
    <p>These contrasts, for me, provide the distinctively satisfying texture of South Korean urban life. Even Seoul’s seemingly unquenchable thirst for the new coexists with a difficult history that at once feels immediately present and impossibly distant. The country – and doubly so its distilled form, Seoul – has come an unfathomably long way in a matter of decades. You see it not just in its architecture, infrastructure and connection to the wider world, but in its people: I’ve met worldly, stylish Koreans who have clear childhood memories of begging American GIs for chocolate.</p>
    <p>I can draw few connections with other cities built in the 20th century, which as a rule have turned out less well-connected and use space far less efficiently. Apart from a selection of historical tourist attractions, Seoul really did go up in the past 60 years, its country having had to build up more or less all of its wealth, infrastructure and population after the Korean War, which destroyed or depleted all three.</p>

    <img src="../Page_03/Page_03_Images/page_08.jpeg" />

    <p>Most of that startling progress came in the 30 years between the mid-1960s and the Asian Financial Crisis of the mid-90s, an impossibly short timeframe by the standards of western development. Friends who were here more than 20 years ago remember walking the streets of Seoul at all hours, seeing and hearing nothing but construction.</p>
    <p>South Korea is one of the few developed-world cultures which regards change as, for the most part, unequivocally good. This makes Seoul that rarest of places, a world-class city not hobbled by an instinct for the preservation of its own perceived heyday. Of cities trying to freeze themselves in amber, obvious examples abound in Europe, and even the once recklessly-inspiring America has followed suit: whenever I go to San Francisco, I am saddened by its willingness to squander its future for the sake of a handful of Victorian houses.</p>
    <p>The neophiliac Seoul suffers no such qualms. My daily walks along its high streets take me past countless construction sites, many of them as busy in the middle of the night as in the middle of the day. Small- or large-scale, public or private, most of these sites surround themselves with barriers emblazoned with the same words: anjeon cheil (safety first). This imperative has taken on the grimmest kind of irony in the wake of April’s sinking of the MV Sewol.</p>
    <p>That disaster, which claimed 293 lives (many of them high school students from Ansan, just south of Seoul) with another 11 still missing, plunged the Koreans I know at home and abroad into a period of reflection and regret. Even as they ask how such a thing could have happened, they uniformly describe it as the inevitable consequence of the same forces – the unceasing drive to push ahead, the tendency to cut corners, the disregard for the law – that brought South Korea up from nothing in the first place.</p>

    <img src="../Page_03/Page_03_Images/page_09.jpeg" />

    <p>The Sewol was not an isolated incident but the latest – and not the deadliest – in a long series of negligence-related tragedies. Some 502 people died when Seoul’s Sampoong Department Store suddenly collapsed in 1995, taking with it, in the words of Korean-American writer Krys Lee, “generations of families”. The list goes on: preventable Korean Air crashes, the Seongsu Bridge’s collapse over Seoul’s Han River, subway fires, auditorium cave-ins, and every other contributor to this country of 50 million’s astonishing 31,000 accidental deaths per year. Koreans must surely wonder if they’ve built a truly developed country or a dysfunctional sham, with Seoul a blindingly illuminated, distraction-filled Potemkin village.</p>
    <p>And yet, Seoul stokes my urbanist passions like nowhere else. It has faced down challenges that many longer-established cities of the west have yet to deal with, or in some cases even acknowledge: accommodating an explosion in urban population; designing for density without relying on an existing, pre-car built environment; and retaining without fetishising elements of its past.</p>
    <p>And then there’s the ‘candle-burnt-at-both-ends’ Seoul lifestyle – at once workaholic and hedonistic, drifting and familial. By assuming the role of ‘gonzo urbanist’, immersing myself in the triumphs, mis-steps and sometimes downright bizarre experiments that characterise this country’s urban future, I can figure out just what it offers me that better-known, longer-established centres of world commerce and culture don’t.</p>
    <p>Throughout this week, you can live that experience here with me at Guardian Cities. So as they say here, gapshida – let’s go.</p>

  </div>
</body>

</html>