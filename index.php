<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="UFT-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PGDCA Project Website</title>
  <link rel="stylesheet" href="css/style.css"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="navbar.js"></script>
 </head>
 <body>
  <section class="main" id="home">
    <nav class="navbar-fixed-top">
      <a href="#home" class="logo">
        <img src="images/logo.png"/>
      </a>

      <ul class="menu">
        <li><a href="#home" class="active">Home</a></li>
        <li><a href="#features">Features</a></li>
        <li><a href="#about">About</a></li>
         <li><a href="#feedback">Feedback</a></li>
        <li><a href="#footer">Contact Us</a></li>

      </ul>
    </nav>
    <div class="main-heading">
      <h1>Inssa Korea</h1>
      <p>Split by a hair-trigger border, the Korean Peninsula offers the traveler a dazzling range of experiences, beautiful landscapes and 5000 years of culture and history.</p>
    </div>
  </section>

  <section id="features">
  <div class="features01">
    <div class="feature-container">

      <div class="feature-box">
        <div class="f-img">
          <img src="images/feature_img_01.jpg"/>
        </div>
        <div class="f-text">
          <h4>Amazing Geography</h4>
          <a href="Page_01/page_01.html" class="main-btn" target="_blank">Read More</a>
        </div>
      </div>

      <div class="feature-box">
        <div class="f-img">
          <img src="images/feature_img_02.jpg"/>
        </div>
        <div class="f-text">
          <h4>Food Feast</h4>
          <a href="Page_02/page_02.html" class="main-btn" target="_blank">Read More</a>
        </div>
      </div>

      <div class="feature-box">
        <div class="f-img">
          <img src="images/feature_img_03.jpg"/>
        </div>
        <div class="f-text">
          <h4>Futuristic Modernity</h4>
          <a href="Page_03/page_03.html" class="main-btn" target="_blank">Read More</a>
        </div>
      </div>

    </div>
  </div>

  <div class="features02">
    <div class="feature-container">

      <div class="feature-box">
        <div class="f-img">
          <img src="images/feature_img_04.jpg"/>
        </div>
        <div class="f-text">
          <h4>Hallyu Wave</h4>
          <a href="Page_04/page_04.html" class="main-btn" target="_blank">Read More</a>
        </div>
      </div>

      <div class="feature-box">
        <div class="f-img">
          <img src="images/feature_img_05.jpg"/>
        </div>
        <div class="f-text">
          <h4>Make-Up & Skincare</h4>
          <a href="Page_05/page_05.html" class="main-btn" target="_blank">Read More</a>
        </div>
      </div>

      <div class="feature-box">
        <div class="f-img">
          <img src="images/feature_img_06.jpg"/>
        </div>
        <div class="f-text">
          <h4>Quirky Café</h4>
          <a href="Page_06/page_06.html" class="main-btn" target="_blank">Read More</a>
        </div>
      </div>

    </div>
  </div>

  <div class="features03">
    <div class="feature-container">

      <div class="feature-box">
        <div class="f-img">
          <img src="images/feature_img_07.jpg"/>
        </div>
        <div class="f-text">
          <h4>Stunning Coastline</h4>
          <a href="Page_07/page_07.html" class="main-btn" target="_blank">Read More</a>
        </div>
      </div>

      <div class="feature-box">
        <div class="f-img">
          <img src="images/feature_img_08.jpg"/>
        </div>
        <div class="f-text">
          <h4>Pulsating Culture</h4>
          <a href="Page_08/page_08.html" class="main-btn" target="_blank">Read More</a>
        </div>
      </div>

      <div class="feature-box">
        <div class="f-img">
          <img src="images/feature_img_09.jpg"/>
        </div>
        <div class="f-text">
          <h4>Korea Trendsetters</h4>
          <a href="Page_09/page_09.html" class="main-btn" target="_blank">Read More</a>
        </div>
      </div>

    </div>
  </div>
  </section>

  <section class="about" id="about">
    <div class="about-img">
      <img src="images/about.jpeg"/>
    </div>
    <div class="about-text">
      <h2>brush up some history of the country</h2>
      <p>This article is an overview of the history of Korea, up to the division of Korea in the 1940s. See History of North Korea and History of South Korea for the post-World War II period. See also Names of Korea.</p>
      <button class="main-btn">
        <a href="https://www.newworldencyclopedia.org/entry/History_of_Korea" target="_blank">Read More</a>
      </button>
    </div>
  </section>

  <section class="feedback" id="feedback">
    <div class="feedback-heading">
      <h1>Feedback</h1>
      <p>Didn't find what you're looking for? Feel free to reach out to us!.</p>
    </div>
    <form action="userinformation.php" method="post">
      <input type="text" name="user" placeholder="Your Full Name"/>
      <input type="email" name="email" placeholder="Your Email Address"/>
      <textarea type ="message" name="message" placeholder="Type Your Message Here......."></textarea>
      <button class="main-btn feedback-btn" type="submit">Continue</button>
    </form>
  </section>

  <section id="footer">
  <div class="footer container">
    <div class="brand">
      <div class="brand_01">
              <h1>친구 랑</h1>
              <h2>Anything can be achieved</h2>
      </div>
      <div class="brand_02">
        <div class="social-icon">
          <div class="social-item">
            <a href="mailto:shivangi.saroha20@gmail.com?cc=reachdikshasharma@gmail.com"><img src="https://img.icons8.com/color/48/000000/gmail--v2.png" alt="Gmail Icon" /></a>
          </div>
          <div class="social-item">
            <a href="https://www.instagram.com"><img src="https://img.icons8.com/fluent/48/000000/instagram-new.png" alt="Instagram Icon" /></a>
          </div>
          <div class="social-item">
            <a href="https://www.twitter.com"><img src="https://img.icons8.com/fluent/48/000000/twitter.png" alt="Twitter Icon" /></a>
          </div>
          <div class="social-item">
            <a href="https://www.linkedin.com"><img src="https://img.icons8.com/fluent/48/000000/linkedin.png" alt="LinkedIn Icon" /></a>
          </div>
        </div>
        <p>Copyright &#169; 2021 Diksha & Shivangi. All rights reserved</p>
      </div>
    </div>
  </div>
</section>
 </body>
</html>